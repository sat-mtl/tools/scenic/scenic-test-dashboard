const { merge } = require('webpack-merge')
const path = require('path')

const Html = require('html-webpack-plugin')
const Dotenv = require('dotenv-webpack')
const { CleanWebpackPlugin: Clean } = require('clean-webpack-plugin')

const TARGET = process.env.npm_lifecycle_event

const __root = path.resolve(__dirname, '..')
const __dist = path.resolve(__root, 'dist')
const __js = path.resolve(__root, 'src/index.js')
const __html = path.resolve(__root, 'res/index.html')

const common = {
  entry: __js,
  output: {
    filename: '[name].bundle.js',
    path: __dist
  },
  resolve: {
    alias: { '~': path.resolve(__root, 'src') }
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader'
    }, {
      test: /\.css$/i,
      use: ['style-loader', 'css-loader'],
    }, {
      test: /\.(woff|woff2|eot|ttf|otf)$/,
      loader: 'file-loader'
    }]
  },
  plugins: [
    new Dotenv({ safe: true, systemvars: true }),
    new Clean(),
    new Html({ template: __html })
  ]
}

if (TARGET === 'start') {
  module.exports = merge(common, {
    mode: 'development',
    devtool: 'eval-cheap-module-source-map',
    devServer: {
      static: {
        directory: __dist
      }
    }
  })
} else {
  module.exports = common
}
