import React from 'react'
import JSONInput from 'react-json-editor-ajrm'

import { H3 } from '@blueprintjs/core'

function JSONEditor ({ data }) {
  if (typeof data !== 'object') {
    data = {}
  }

  return (
    <div className='json-editor'>
      <H3>JSON Editor</H3>
      <JSONInput
        placeholder={data} // data to display
        width='800px'
        height='100%'
        viewOnly='true'
      />
    </div>
  )
}

export default JSONEditor
