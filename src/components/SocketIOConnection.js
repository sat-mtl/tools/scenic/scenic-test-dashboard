import React, { useState } from 'react'
import PropTypes from 'prop-types'

import { Button, ControlGroup, H3, HTMLSelect, InputGroup } from '@blueprintjs/core'

function SocketIOConnection (props) {
  const { connected, connect, disconnect } = props
  const [protocol, setProtocol] = useState('http')
  const [host, setHost] = useState('localhost')
  const [port, setPort] = useState('8000')

  const onClick = () => {
    if (!connected) {
      connect(protocol, host, port)
    } else {
      disconnect()
    }
  }

  return (
    <div className='io-connection-form'>
      <H3>SocketIO Connection</H3>
      <ControlGroup fill>
        <HTMLSelect
          disabled={connected}
          value={protocol}
          onChange={(e) => { setProtocol(e.target.value) }}
          options={['http', 'https']}
        />
        <InputGroup
          placeholder='host'
          value={host}
          disabled={connected}
          onChange={(e) => setHost(e.target.value)}
        />
        <InputGroup
          placeholder='port'
          value={port}
          disabled={connected}
          onChange={(e) => setPort(e.target.value)}
        />
        <Button
          intent={!connected ? 'primary' : 'warning'}
          onClick={onClick}
        >
          {!connected ? 'Connect' : 'Disconnect'}
        </Button>
      </ControlGroup>
    </div>
  )
}

SocketIOConnection.propTypes = {
  connected: PropTypes.bool,
  connect: PropTypes.func,
  disconnect: PropTypes.func
}

SocketIOConnection.defaultProps = {
  connected: false,
  connect: Function.prototype,
  disconnect: Function.prototype
}

export default SocketIOConnection
