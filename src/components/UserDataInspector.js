import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { tree, quiddity } from '@sat-mtl/scenic-api'

import { Button, FormGroup, ControlGroup, H4, HTMLSelect, InputGroup } from '@blueprintjs/core'

const { InfoTreeAPI, UserTreeAPI, ConnectionSpecsAPI } = tree
const { NicknameAPI } = quiddity

const Methods = Object.freeze({
  INFOTREE_GET: 'infoTree.get',
  USERTREE_GET: 'userTree.get',
  NICKNAME_GET: 'nickname.get',
  CONNECTION_SPEC_GET: 'connectionSpec.get'
})

async function emitMethod (socket, updateData, method, quidId, branch) {
  console.log(`Send ${method} for ${quiddity}`)

  switch (method) {
    case Methods.INFOTREE_GET: {
      const api = new InfoTreeAPI(socket)

      try {
        console.log(quidId)
        const tree = await api.get(quidId, branch)
        console.log(tree)
        updateData(tree)
      } catch (error) {
        console.error(`${method} triggered an error: ${error}`)
        console.trace(error)
      }

      break
    }

    case Methods.USERTREE_GET: {
      const userTreeAPI = new UserTreeAPI(socket)

      try {
        updateData(await userTreeAPI.get(quidId, branch))
      } catch (error) {
        console.error(`${method} triggered an error: ${error}`)
        console.trace(error)
      }

      break
    }

    case Methods.NICKNAME_GET: {
      const nicknameAPI = new NicknameAPI(socket)

      try {
        updateData({ nickname: await nicknameAPI.get(quidId) })
      } catch (error) {
        console.error(`${method} triggered an error: ${error}`)
        console.trace(error)
      }

      break
    }

    case Methods.CONNECTION_SPEC_GET: {
      const api = new ConnectionSpecsAPI(socket)

      try {
        console.log(quidId)
        updateData(await api.get(quidId, branch))
      } catch (error) {
        console.error(`${method} triggered an error: ${error}`)
        console.trace(error)
      }

      break
    }

    default: {
      console.warn('Sent nothing')
      break
    }
  }
}

function BranchInput ({ method, disabled, branch, setBranch }) {
  return (
    <FormGroup className='branch-input' label='UserData Branch'>
      <InputGroup
        placeholder='branch'
        value={branch}
        disabled={disabled}
        onChange={(e) => setBranch(e.target.value)}
      />
    </FormGroup>
  )
}

function UserDataInspector (props) {
  const { disabled, socket, availableQuids, updateAvailableQuids, updateData } = props

  const [method, setMethod] = useState(Methods.INFOTREE_GET)
  const [quidId, setQuiddity] = useState(availableQuids[0]?.id || '')
  const [branch, setBranch] = useState('')

  useEffect(() => {
    setQuiddity(availableQuids[0]?.id)
  }, [availableQuids])

  const onClick = () => emitMethod(socket, updateData, method, quidId, branch)

  return (
    <div className='user-data-inspector'>
      <H4>UserData Inspector</H4>
      <div className='user-data-inspector-layout'>
        <FormGroup className='method-select' label='Method'>
          <HTMLSelect
            disabled={disabled}
            value={method}
            options={Object.values(Methods)}
            onChange={(e) => setMethod(e.target.value)}
          />
        </FormGroup>
        <FormGroup className='quiddity-select' label='Quiddity ID'>
          <ControlGroup>
            <HTMLSelect
              disabled={disabled}
              value={quidId}
              options={availableQuids.map(q => ({
                label: `${q.kind} - ${q.id} `,
                value: q.id
              }))}
              onChange={(e) => { console.log(e.target); setQuiddity(e.target.value)}}
            />
            <Button
              disabled={disabled}
              icon='refresh'
              onClick={updateAvailableQuids}
            />
          </ControlGroup>
        </FormGroup>
        {[Methods.USERTREE_GET, Methods.INFOTREE_GET].includes(method) && (
          <BranchInput
            method={method}
            disabled={disabled || !quidId}
            branch={branch}
            setBranch={setBranch}
          />
        )}
      </div>
      <Button
        disabled={disabled || !quidId}
        onClick={onClick}
      >
        Send
      </Button>
    </div>
  )
}

UserDataInspector.propTypes = {
  disabled: PropTypes.bool,
  socket: PropTypes.any,
  availableQuids: PropTypes.arrayOf(PropTypes.object),
  updateAvailableQuids: PropTypes.func,
  updateData: PropTypes.func
}

UserDataInspector.defaultProps = {
  disabled: true,
  socket: null,
  availableQuids: [],
  updateAvailableQuids: [],
  updateData: Function.prototype
}

export default UserDataInspector
