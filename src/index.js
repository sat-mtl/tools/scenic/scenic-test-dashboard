import React, { Component } from 'react'
import { createRoot } from 'react-dom/client';
import { quiddity } from '@sat-mtl/scenic-api'
import io from 'socket.io-client';

import { H3 } from "@blueprintjs/core"

import '@blueprintjs/icons/lib/css/blueprint-icons.css'
import '@blueprintjs/core/lib/css/blueprint.css'

import JSONEditor from './components/JSONEditor'
import SocketIOConnection from './components/SocketIOConnection'
import UserDataInspector from './components/UserDataInspector'

import './layout.css'

const { QuiddityAPI } = quiddity

class App extends Component {
  _socket = null

  state = {
    connected: false,
    availableQuids: [],
    data: null
  }

  get socket () { return this._socket }

  connect (protocol, host, port) {
    this._socket = io(`${protocol}://${host}:${port}`)

    this.socket.on('connect', () => {
      this.setState({ connected: true })
    })

    this.socket.on('disconnect', () => {
      this.setState({ connected: false })
    })

    this.socket.on('quiddity.userData.updated', (quidId, path, data) => {
      console.log(`userData has been updated for ${quidId} at ${path}`)
      console.log(data)
      //this.setState({ data: data })
    })
  }

  async fetchQuiddityList () {
    const { availableQuids } = this.state
    const quiddityAPI = new QuiddityAPI(this.socket)

    try {
      const list = await quiddityAPI.listCreated()

      if (list.length !== availableQuids.length) {
        this.setState({ availableQuids: list })
      }
    } catch (error) {
      console.error('Failed to fetch quiddity list')
      console.trace(error)
    }
  }

  disconnect () {
    if (this._socket) {
      this.socket.disconnect()
      this._socket = null
    }
  }

  componentDidUpdate () {
    if (this.socket) {
      this.fetchQuiddityList()
    }
  }

  render () {
    const { connected, data, availableQuids } = this.state
    const disabledInfo = !connected ? '(disabled)' : ''
    const connect = this.connect.bind(this)
    const disconnect = this.disconnect.bind(this)

    return (
      <div className='layout'>
        <JSONEditor data={data} />

        <div className='inspector'>
          <SocketIOConnection
            connected={connected}
            connect={connect}
            disconnect={disconnect}
          />

          <div className='switcher-ctrl-form'>
            <H3>API Form {disabledInfo}</H3>
            <UserDataInspector
              disabled={!connected && availableQuids.length === 0}
              socket={this.socket}
              availableQuids={availableQuids}
              updateAvailableQuids={() => this.fetchQuiddityList()}
              updateData={(data) => this.setState({ data: data })}
            />
          </div>
        </div>
      </div>
    )
  }
}

const root = createRoot(document.getElementById('app'))
root.render(<App />)
