# Scenic Test Dashboard

Simple UI based on [blueprintjs](https://blueprintjs.com) used to test the API through [SocketIO](https://socket.io) of [Scenic Core](https://gitlab.com/sat-mtl/telepresence/scenic-core) and [Switcher Control](https://gitlab.com/sat-metalab/switcher).

## Getting started

Launch the client from its root folder :

```bash
git clone git@gitlab.com:sat-mtl/telepresence/prototypes/scenic-test-dashboard.git
cd scenic-test-dashboard

npm install
npm start
```

You'll need to start a Switcher API before testing.
